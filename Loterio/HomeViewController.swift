//
//  HomeViewController.swift
//  Loterio
//
//  Created by admin on 5/16/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class HomeViewController: UIViewController {
    // MARK: *** Local variables

    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    
    // MARK: *** UI Events

    override func viewDidLoad() {
        super.viewDidLoad()
        let navBar = self.navigationController!.navigationBar
        navBar.barTintColor = UIColor(rgb: 0x3B577D)
        navBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        // Define the menus
        setupSideMenu()
        getMenus()
    }
    override func viewDidAppear(_ animated: Bool) {
        print("HomeVC: viewDidAppear")
    }
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        //SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    func getMenus(){
        Alamofire.request(AppConstant.urlMenus).responseJSON { response in
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
        }
    }
}


//
//  AppConstant.swift
//  Loterio
//
//  Created by admin on 5/20/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import Foundation

class AppConstant{
    public static let urlMenus : String = "https://shielded-springs-57373.herokuapp.com/menus"
    public static let urlCities : String = "https://shielded-springs-57373.herokuapp.com/cities"
    public static let urlBranches : String = "https://shielded-springs-57373.herokuapp.com/branches"
    public static let urlEvents : String = "https://shielded-springs-57373.herokuapp.com/events"
    
    // user sign up
    public static let urlUserSignUp : String = "https://shielded-springs-57373.herokuapp.com/user"
    // user log in
    public static let urlUserLogin : String = "https://shielded-springs-57373.herokuapp.com/user/login"
    //user favorites
    public static let urlUserFavorite : String = "https://shielded-springs-57373.herokuapp.com/favorite"
    
    
    //for each food list
    public static let urlBurgers : String = "https://shielded-springs-57373.herokuapp.com/menus/burgers"
    public static let urlCombos : String = "https://shielded-springs-57373.herokuapp.com/menus/combos"
    public static let urlValues : String = "https://shielded-springs-57373.herokuapp.com/menus/values"
    public static let urlChickens : String = "https://shielded-springs-57373.herokuapp.com/menus/chickens"
    public static let urlRices : String = "https://shielded-springs-57373.herokuapp.com/menus/rices"
    public static let urlDesserts : String = "https://shielded-springs-57373.herokuapp.com/menus/desserts"
    public static let urlDrinks : String = "https://shielded-springs-57373.herokuapp.com/menus/drinks"
}

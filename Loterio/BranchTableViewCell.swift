//
//  BranchTableViewCell.swift
//  Loterio
//
//  Created by Hai Nguyen on 5/26/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit

class BranchTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneTextView: UITextView!
    @IBOutlet weak var timeAvailableTextView: UITextView!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var nameTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  SideMenuViewController.swift
//  Loterio
//
//  Created by admin on 5/17/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class SideMenuViewController: UIViewController {
    
    // MARK: *** Local variables
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    // MARK: *** UI Events
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        print("Log out")
        let token = UserDefaults.standard.string(forKey: "token")
        if token != nil{
            // for log out
            let loginManager = LoginManager()
            loginManager.logOut()
            UserDefaults.standard.set(nil, forKey: "token")
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"LoginVC")
            present(viewController, animated: true)
        }else{
            // user did logout
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Side menu : view did appear")
    }
}

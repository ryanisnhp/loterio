//
//  ViewController.swift
//  Loterio
//
//  Created by admin on 3/14/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    // MARK: *** Local variables
    let permissionsToRead = ["public_profile", "email"]
    let graphRequestParameters = ["fields": "id, email"]
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var loginButton: FBSDKLoginButton!
    
    // MARK: *** UI Events
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        self.loginButton.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.readPermissions = permissionsToRead
        let token = UserDefaults.standard.string(forKey: "token")
        if token != nil{
            graphRequestToReturnUserData(graphParameters: graphRequestParameters)
        }else{
            print("User logged out")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let token = UserDefaults.standard.string(forKey: "token")
        if token != nil{
            moveToHome()
        }else{
            print("User logged out")
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if ((error) != nil) {
            // Process error
            print("Error")
        }
        else if result.isCancelled {
            // Handle cancellations
            print("Cancelled")
        }
        else {
            // Navigate to other view
            print("Success")
            if result.grantedPermissions.contains("email") {
                graphRequestToReturnUserData(graphParameters: graphRequestParameters)
                moveToHome()
            }
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Out")
        UserDefaults.standard.set(nil, forKey: "token")
    }
    
    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>) {
        FBSDKGraphRequest(graphPath: "me", parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                print(fbDetails)
                print(FBSDKAccessToken.current().tokenString)
            }
        })
    }
    func moveToHome() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"rootViewController")
        present(viewController, animated: false)
    }
}


//
//  DiscoveryViewController.swift
//  Loterio
//
//  Created by Hai Nguyen on 5/26/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class DiscoveryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var branchesTableView: UITableView!
    
    @IBOutlet weak var dropDownCityButton: UIButton!
    
    @IBAction func dropDownCityButtonTapped(_ sender: Any) {
        chooseCityDropDown.show()
        chooseCityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dropDownCityButton.setTitle("Choose City: \(item)", for: .normal)
            self.loadBranches(idCity: index)
        }
    }
    
    let chooseCityDropDown = DropDown()
    var citiesData: [[String: AnyObject]] = []
    var branchesData: [[String: AnyObject]] = []
    var noBranchesLabel: UILabel = UILabel()
    var isFirst: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        noBranchesLabel.text = "Please choose city first"
        noBranchesLabel.textAlignment = .center
        noBranchesLabel.frame = CGRect(x: 0, y: 0, width: 300, height: 40)
        noBranchesLabel.isEnabled = false
        self.view.addSubview(noBranchesLabel)
        noBranchesLabel.center = self.view.center
        
        branchesTableView.dataSource = self
        branchesTableView.delegate = self
        
        chooseCityDropDown.anchorView = dropDownCityButton
        chooseCityDropDown.reloadAllComponents()
        loadCities()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (branchesData.count == 0) {
            self.branchesTableView.isHidden = true
            if isFirst == 0 {
                self.noBranchesLabel.isHidden = false
                noBranchesLabel.text = "Internet connection problem"
            }
            
        }
        else {
            self.branchesTableView.isHidden = false
            self.noBranchesLabel.isHidden = true
            isFirst = 0
        }
        return (branchesData.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell", for: indexPath) as! BranchTableViewCell
        if !(branchesData[indexPath.row]["name"] is NSNull) {
            cell.nameTextView.text = branchesData[indexPath.row]["name"] as! String
        }
        else {
            cell.nameTextView.text = "Not available yet"
        }
        
        if !(branchesData[indexPath.row]["address"] is NSNull) {
            cell.addressTextView.text = branchesData[indexPath.row]["address"] as! String
        }
        else {
            cell.addressTextView.text = "Not available yet"
        }
        
        if !(branchesData[indexPath.row]["phone_number"] is NSNull) {
            
            cell.phoneTextView.text = branchesData[indexPath.row]["phone_number"] as! String
        }
        else {
            cell.phoneTextView.text = "Not available yet"
        }
        
        if !(branchesData[indexPath.row]["time_available"] is NSNull) {
            cell.timeAvailableTextView.text = branchesData[indexPath.row]["time_available"] as! String
        }
        else {
            cell.timeAvailableTextView.text = "Not available yet"
        }
        
        return cell
    }
    
    func loadCities() {
        var cityNameArray = [String]()
        Alamofire.request(AppConstant.urlCities).responseJSON(completionHandler: {
            (responseData) -> Void in
            if responseData.result.value != nil {
                let jsonCitiesData = JSON(responseData.result.value!)
                
                if let resData = jsonCitiesData.arrayObject {
                    self.citiesData = (resData as? [[String: AnyObject]])!
                    for i in 0...(self.citiesData.count - 1) {
                        cityNameArray.append(self.citiesData[i]["name"] as! String)
                        Alamofire.request(AppConstant.urlBranches + "?id=" + "\((self.citiesData[i]["id"] as! Int))").responseJSON(completionHandler: {
                            (responseData) -> Void in
                            if responseData.result.value != nil {
                            }
                        })
                    }
                    self.chooseCityDropDown.dataSource = cityNameArray
                }
            }
        })
        
    }
    
    func loadBranches(idCity: Int) {
        
        Alamofire.request(AppConstant.urlBranches + "?id=" + "\((self.citiesData[idCity]["id"] as! Int))").responseJSON(completionHandler: {
            (responseData) -> Void in
            if responseData.result.value != nil {
                let jsonBranchesData = JSON(responseData.result.value!)
                
                if let resData = jsonBranchesData.arrayObject {
                    self.branchesData = (resData as? [[String: AnyObject]])!
                    self.branchesTableView.reloadData()
                }
            }
        })
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
